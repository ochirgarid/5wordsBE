<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public function getLevelAttribute($value)
    {
        if($value){
            return $value;
        }
        return 1;
    }
}
