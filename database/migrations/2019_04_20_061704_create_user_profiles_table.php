<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfilesTable extends Migration {

	public function up()
	{
		Schema::create('user_profiles', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->smallInteger('level');
		});
	}

	public function down()
	{
		Schema::drop('user_profiles');
	}
}