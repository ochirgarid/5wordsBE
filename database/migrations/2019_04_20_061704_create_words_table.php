<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWordsTable extends Migration {

	public function up()
	{
		Schema::create('words', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('word');
			$table->integer('language_id');
			$table->smallInteger('level');
		});
	}

	public function down()
	{
		Schema::drop('words');
	}
}